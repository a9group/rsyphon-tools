# Rsyphon Tools #

These are a few unsupported tools for working with Rsyphon.

### What is this repository for? ###

* rs_mkimagepackage

** Image packager for portability between image servers

* rs_initrdtools

** Initrd mounter, editor for quick and dirty changes to support oddball use cases. Supplements 'Standard' builds for PXE, local media boot